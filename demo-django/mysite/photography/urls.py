from django.contrib import admin
from django.urls import path
from .views import home_views, about_views,awards_views,contact_views

app_name = "photography"
urlpatterns = [
    path('', home_views.index),
    path('home', home_views.index, name='home'),
    path('about', about_views.index, name='about'),
    path('awards', awards_views.index, name='awards'),
    path('contact', contact_views.index, name='contact'),

]
