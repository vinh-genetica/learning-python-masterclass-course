from django.http import Http404
from django.shortcuts import render
from ..models import *
import math
from django.core.paginator import Paginator


def index(request):
    all_photos = Photo.objects.all()
    all_photos_pages = Paginator(all_photos, 8)
    all_photos_pages.page_list = []
    for i in all_photos_pages.page_range:
        page = all_photos_pages.page(i)
        all_photos_pages.page_list.insert(len(all_photos_pages.page_list), page)
    context = {
        'all_photos_pages': all_photos_pages,
    }
    return render(request, "home.html", context)
