from django.db import models
from django.conf.urls.static import static


# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name_plural = "Categories"


class Photo(models.Model):
    name = models.CharField(max_length=100)
    image_small = models.ImageField(upload_to='gallery')
    image_large = models.ImageField(upload_to='gallery')
    description = models.CharField(max_length=1000)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return str(self.name)
