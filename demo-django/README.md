# Demo django project


## Overview
- Requirements file: [requirements.txt](requirements.txt)
- Using web template: https://templatemo.com/tm-413-flip-turn
- Website screenshot: 
    - Home page:
    
        ![Preview homepage](images/homepage.png)
    
    - About page:
    
        ![Preview about](images/about.png)
    
    - Awards page:
    
        ![Preview awards](images/awards.png)
        
    - Contact page:
    
        ![Preview contact](images/contact.png)
    
## In demo project

### Django clean-up
- Using for deleted unused media file
- Install
    
    ```
    pip install django-cleanup
    ```
     
    ```
    INSTALLED_APPS = (
         ...
        'django_cleanup', # should go after your apps
    )
    ```
### Customize admin panel
- Customize model name on Django admin panel

    ```
    class Category(models.Model):
    ...
    ...
    class Meta:
        verbose_name_plural = "Categories"
    ```
- Screen shoot:
    - Before:
    
    ![Preview admin_before](images/admin_before.png)
    
    - After
    
    ![Preview admin_after](images/admin_after.png)
    
### Database Models
- Using relationship to drop Photo record when Category is deleted
    ```
    class Photo(models.Model):
    ...
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1)
    ...
    ```
- Using *ImageField* to uploading and using image on the website
    - Install 
        ```
        pip install Pillow
        ```
    - Set *MEDIA_ROOT* and *MEDIA_URL* property to setting.py      
        ```
        MEDIA_ROOT = os.path.join(BASE_DIR, '')
        MEDIA_URL = '/gallery/'
        ```
    - Using in models
        ```
        class Photo(models.Model):
        ...
        image_small = models.ImageField(upload_to='gallery')
        image_large = models.ImageField(upload_to='gallery')
        ...
      ```
    - Set urls so usersite can view the image
    ```
      urlpatterns = [
                      ...
                      ...
                  ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  ```
      
- [Uploaded media folder](\demo-django\mysite\gallery) 
         
###  Implement Template:
- Directory

    ![Preview directory](images/directory.png)
    
- Base layout
- Using static for template image,css,js
    ```
    {% load static %}
    <!DOCTYPE html>
    <html lang="en">
    <head>
        ...
        ...
        <link href="{% static 'template413/css/font-awesome.min.css' %}" rel="stylesheet" type="text/css">
        <link href="{% static 'template413/css/magnific-popup.css' %}" rel="stylesheet">
        <link href="{% static 'template413/css/templatemo_style.css' %}" rel="stylesheet" type="text/css">
    </head>
  ```
  
### Paginator
- Using *Paginator*
- [home_views.py](\demo-django\mysite\photography\views\home_views.py)     
    
    ```
    def index(request):
    # Get all photo queryset in the database
    all_photos = Photo.objects.all() 
  
    # Divide list of photo to pages 
    # Each page have 8 photo
    all_photos_pages = Paginator(all_photos, 8) 
    
    # Set data for the list pages
    all_photos_pages.page_list = []
    for i in all_photos_pages.page_range:
        page = all_photos_pages.page(i)
        all_photos_pages.page_list.insert(len(all_photos_pages.page_list), page)
  
    # Set data for context
    context = {
        'all_photos_pages': all_photos_pages,
    }
    
    # Respond
    return render(request, "home.html", context)
    ```
 - [home.html](\demo-django\mysite\photography\templates\home.html)  
    
    ```
    <div id="portfolio-content" class="center-text">
            {% if all_photos_pages %} 
                {% for page in all_photos_pages.page_list %}
                    <div class="portfolio-page"
                         id="page-{{ forloop.counter }}"
                         {% if forloop.counter != 1 %}style="display:none;"{% endif %}>
                        {% for photo in page.object_list %}     <!--Get a photo from list photo of current page-->
                            <div class="portfolio-group">
                                <a class="portfolio-item" href="{{ photo.image_large.url }}">   <!--Load photo image-->
                                    ...
                                    ...
                                    ...
                                </a>
                            </div>
                        {% endfor %}
                    </div>
                {% endfor %}
                <div class="pagination">
                    <ul class="nav">
                        {% for i in all_photos_pages.page_range %} <!--Loop for the number of pages-->
                            {% if i == 1 %}
                                <li class="active">{{ i }}</li>
                            {% else %}
                                <li>{{ i }}</li>
                            {% endif %}
                        {% endfor %}
                    </ul>
                </div>
            {% endif %}
        </div>
    ```
  
## Outside demo project
### Debugging django backend using Pycharm
- Step 1: Runserver
    ```
  python manage.py runserver
  ```
- Step 2: Attach to process

    ![Preview debug_1](images/debug_1.png)
    
- Step 3: choose process to debug
    
    ![Preview debug_2](images/debug_2.png)
    
- Step 4: Set break point and start debugging

    ![Preview debug_3](images/debug_3.png)

    
    
