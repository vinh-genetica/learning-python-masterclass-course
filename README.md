# The Complete Python Masterclass: Learn Python From Scratch - July 2020

Journal for Learning Course: [The Complete Python Masterclass: Learn Python From Scratch](https://www.udemy.com/course/python-masterclass-course/)

## Planning
- Expected Time: 100 hours
- Finish day: 02/08/2020

## Day 0 (15/07/2020)
* Setup project skeleton

## Day 1 (16/07/2020)
### Today's progress
- Went through course introduction (Section 1)
- Learned basic Python concepts (Section 2), control structure (Section 3)
- Learned basic about function and modules (Section 4)
- Learned basic exception and file handling (Section 5)
- Learned about some type of data in python (Section 6)

### Thoughts
- None

### Links to works
- [Section 1](section-1-introduction/README.md)
- [Section 2](section-2-basic-python-concepts/README.md)
- [Section 3](section-3-control-structures-in-python/README.md)
- [Section 4](section-4-functions-and-modules-in-python/README.md)
- [Section 5](section-3-control-structures-in-python/README.md)
- [Section 6](section-6-some-more-types-in-python/README.md)

## Day 2 (17/07/2020)
### Today's progress
- Learned about function programing in Python (Section 7)
    - Lambdas
    - Map, Filter, Reduce
    - Generator Functions and Generator Expressions   
- Learned about OOP, inheritance, recursion,encapsulation, Operator overload (Section 8)
- Itertools, Sets and sets operation (Section 8)
- Brief about RegEx (Section 9)
### Thoughts
- Itertools is such powerful tools for coding and memory efficacy   

### Links to works
- [Section 7](section-7-function-programming-in-python/README.md)
- [Section 8](section-8-object-oriented-programming-in-python/README.md)
- [Section 8](section-9-regular-expressions-in-python/README.md)

## Day 3 (18/07/2020)
### Today's progress
- Learned about Basic GUI apps with Tkinter (Section 10)
### Thoughts
- None

### Links to works
- [Section 10](section-10-create-gui-apps-in-python-using-tkinter/README.md)

## Day 4 (19/07/2020)
### Today's progress
- Create simple calculator application using Tkinter (Section 11)
### Thoughts
- None

### Links to works
- [Section 11](section-11-building-calculator-app-using-tkinter/README.md)

## Day 5 (20/07/2020)
### Today's progress
- Learned about PostgreSQL (Section 12)
- Built database application using Tkinter and PostgreSQL (Section 12)
- Learned about Anaconda and Pandas (Section 13)
    - Series, Data Frames 
    
### Thoughts
- None

### Links to works
- [Section 12](section-12-building-database-apps-with-postgresql-and-python/README.md)
- [Section 13](section-13-data-analysis-using-python/README.md)

## Day 6 (21/07/2020)
### Today's progress
- Learned about `Anaconda` and `Pandas` (Section 13)
    - Arithmetic operations
    - Sorting
    - Handling duplicate and NaN values
    - Loading data from file
- Learned about `Numpy` array (Section 13)
- Learned how to plotting data using `Matplotlib`(Section 13)
- Analysed Supermarket Sales Data (Section 13)

### Thoughts
- None

### Links to works
- [Section 13](section-13-data-analysis-using-python/README.md)

## Day 7 (22/07/2020)
### Today's progress
- Learned how to make simple web application with `Django 1.11.2` (Section 14)
    - Django models and Database
    - Django admin panel
    - Creating views using template and static file
    - Raising 404 error
    - Genetic view
- Learned how to make simple web application with `Django 2.0` (Section 15)

### Thoughts
- None

### Links to works
- [Section 14](section-14-make-web-applications-in-python-using-django/README.md)
- [Section 15](section-15-make-web-applications-with-python-using-django-2.0/README.md)

## Day 8 (23/07/2020)
### Today's progress
- Practiced what learned about Python and Django

### Thoughts
- Practiced how to implement web Template
- Learned How to use *ImageField*
    - Uploading and Using
    - Handling unused media file
- Learned how to handling paginator with *Paginator*
- Learned how to debug Django backend by using Pycharm

### Links to works
- [Demo Django](demo-django/README.md)

## Day 9 (24/07/2020)
### Today's progress
- Learned about API (Section 17)
- Learned how to build basic REST API using Django REST Framework (Section 17)

### Thoughts
- Introduction of API
- Built basic REST API using Django REST Framework
    - Building basic REST API
    - Creating API Endpoints
    - Adding Image Field 
    - Filtering
    - Search functionality
    - *DefaultRouter* vs *SimpleRouter*
    - API Authentication

### Links to works
- [Section 17](section-17-building-rest-api-with-python-and-django/README.md)

## Day 12 (27/07/2020)
### Today's progress
- Learned how to crawling data from a website (Section 18)
- Complete challenge-01 of Coding challenge (Section 19)
### Thoughts
- Section 18
    - Getting html code of a web page
    - Using BeautifulSoup4 
    - Web crawler introduction
    - Building a web crawler 
- Section 19: 
    - Challenge 1: Tile contractor app
        - Level 1:
            - Create console app to accept input and print calculated output
            - Validate input
        - Level 2: 
            - Using **tkinter** to create GUI app 
            - Validate input
            - Accept values in different units

### Links to works
- [Section 18](section-18-learn-how-to-crawl-websites-using-python-web-crawling/README.md)
- [Section 19](section-19-complex-coding-challenges/README.md)

## Day 13 (28/07/2020)
### Today's progress
- Continue Coding challenge (Section 19)
### Thoughts
- Section 19: 
    - Challenge 2: TAX calculator
    - Challenge 3: Text editor
    - Challenge 4: Product inventory project

### Links to works
- [Section 19](section-19-complex-coding-challenges/README.md)

## Day 14 (29/07/2020)
### Today's progress
- Continue Coding challenge (Section 19)
### Thoughts
- Section 19: 
    - Challenge 6: Build a scientific calculator using Tkinter   
    - Challenge 7: Build a global clock which shows current local timings across the globe

### Links to works
- [Section 19](section-19-complex-coding-challenges/README.md)

## Day 16 (31/07/2020)
### Today's progress
- Continue Coding challenge (Section 19)
- Started learn about automation with Selenium Web Driver and Python (Section 20)
### Thoughts
- Section 19: 
    - Challenge 8: Real time weather app
    - Challenge 9: Web Crawler
- Section 20:
    - Selenium Introduction
    - Selenium Installing
### Links to works
- [Section 19](section-19-complex-coding-challenges/README.md)
- [Section 20](section-20-automation-with-selenium-web-driver-and-python/README.md)

## Day 19 (03/08/2020)
### Today's progress
- Learned about automation with Selenium Web Driver and Python (Section 20)
### Thoughts
- Section 20:
    - Selenium webdriver:
        - Opening a url
        - Automating google search using xpath
        - Clicking links
        - Refreshing a Webpage
        - Using Forward and Backward Navigation Buttons
        - Scrolling and Getting the Current URL
### Links to works
- [Section 20](section-20-automation-with-selenium-web-driver-and-python/README.md)
