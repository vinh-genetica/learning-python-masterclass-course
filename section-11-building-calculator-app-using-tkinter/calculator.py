from tkinter import *
import parser
import itertools
import math

root = Tk()
root.title("calculator")

root.grid_columnconfigure(0, weight=1, uniform="fred")

display = Entry(root)
display.grid(row=0, columnspan=6, sticky=W + E)


def create_button(text, command, grid_row, grid_col):
    Button(root, text=text, command=command).grid(row=grid_row, column=grid_col, sticky=NSEW)


# Append var to the
def get_var(var):
    entry_string = display.get()
    if (entry_string == "Error"):
        clear_all()

    display.insert(len(entry_string), var)


# Clear all
def clear_all():
    display.delete(0, len(display.get()))


# Delete last char of the string
def undo():
    display.delete(-1, 1)


def fac(num):
    return math.factorial(num)


def calculate():
    entry_string = display.get()
    try:
        a = parser.expr(entry_string).compile()
        result = eval(a)
        clear_all()
        display.insert(0, result)
    except Exception:
        clear_all()
        display.insert(0, "Error")


# Create number button
for x, y in itertools.product(range(1, 4), range(0, 3)):
    create_button(str(x + y * 3), lambda num=(x + y * 3): get_var(str(num)), y + 1, x - 1)
    # Button(root, text=str(x + (y * 3)), command=lambda num=(x + y * 3): get_numbers(num)) \
    #     .grid(row=y + 1, column=x - 1, sticky=NSEW)

# Create other button
create_button("AC", lambda: clear_all(), 4, 0)
create_button("0", lambda: get_var("0"), 4, 1)
create_button(".", lambda: get_var("."), 4, 2)

# Create operators button
create_button("+", lambda: get_var("+"), 1, 3)
create_button("-", lambda: get_var("-"), 2, 3)
create_button("*", lambda: get_var("*"), 3, 3)
create_button("/", lambda: get_var("/"), 4, 3)

create_button("PI", lambda: get_var("3.14"), 1, 4)
create_button("%", lambda: get_var("%"), 2, 4)
create_button("(", lambda: get_var("("), 3, 4)
create_button("EXP", lambda: get_var("**"), 4, 4)

create_button("DEL", lambda: undo(), 1, 5)
create_button("x!", lambda: get_var("fac("), 2, 5)
create_button(")", lambda: get_var(")"), 3, 5)
create_button("=", lambda: calculate(), 4, 5)

root.mainloop()
