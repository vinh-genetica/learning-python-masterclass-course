# Section 10:  Create GUI Apps In Python Using Tkinter

## Overview:
- Import tkinter to use
    >     from tkinter import *
    >     root = Tk()
    >     #    
    >     # GUI code here
    >     #
    >     root.mainloop()
- **Grid** vs **Pack**
- **Grid** vs **Pack**
- Adjusting widgets
- Handling button click
- Using class to init GUI windows
- Create simple menu 
    - Dropdown
    - Toolbar
- Status bar
- Simple Message Box
    >     import tkinter.messagebox
- Drawing in Tkinter