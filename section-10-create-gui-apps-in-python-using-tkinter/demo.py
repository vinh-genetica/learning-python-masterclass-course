from tkinter import *
from tkinter import messagebox

root = Tk()

# -------------------
# Hello world
# -------------------

# label1 = Label(root, text="Hello world!")
#
# label1.pack()

# -------------------
# Frame
# -------------------

# newframe = Frame(root)
# newframe.pack()
#
# otherframe = Frame(root)
# otherframe.pack(side=BOTTOM)
#
# button1 = Button(newframe, text="Click here!", fg="Red")
# button2 = Button(otherframe, text="Click here!", fg="Blue")
#
# button1.pack()
# button2.pack()

# -------------------
# Grid, Entry, button click
# -------------------

# label1 = Label(root, text="username")
# label2 = Label(root, text="password")
#
# entry1 = Entry(root)
# entry2 = Entry(root)
#
#
# def login():
#     print("login button clicked!")
#
#
# button1 = Button(root, text="login", command=login)
#
# label1.grid(row=0, column=0)
# label2.grid(row=1, column=0)
# entry1.grid(row=0, column=1)
# entry2.grid(row=1, column=1)
# button1.grid(row=2, column=1, sticky=N + S + E + W)

# -------------------
# Menu: Dropdown , toolbar
# Status Bar
# -------------------

# def project_function():
#     messagebox.showinfo("Info message", "Hello world!")
#     answer = messagebox.askquestion("Question 1", "Do you know de wae?")
#     print(answer)
#     if answer == 'yes':
#         print("You do know de wae!")
#     if answer == 'no':
#         print("You do not know de wae!")
#
#
# mymenu = Menu(root)
# root.config(menu=mymenu)
#
# submenu = Menu(mymenu)
#
# mymenu.add_cascade(label="File", menu=submenu)
#
# submenu.add_command(label="Project", command=project_function)
# submenu.add_command(label="Save", command=project_function)
#
# submenu.add_separator()
#
# submenu.add_command(label="Exit", command=project_function)
#
# submenu_2 = Menu(mymenu)
# mymenu.add_cascade(label="Edit", menu=submenu_2)
#
# submenu_2.add_command(label="Undo", command=project_function)
# submenu_2.add_command(label="Redo", command=project_function)
#
# toolbar = Frame(root, bg="Pink")
# insert_button = Button(toolbar, text="Insert File", command=project_function)
# insert_button.pack(side=LEFT, padx=2, pady=3)
#
# print_button = Button(toolbar, text="Print File", command=project_function)
# print_button.pack(side=LEFT, padx=2, pady=3)
# toolbar.pack(side=TOP, fill=X)
#
# status_bar = Label(root, text="This is the status bar!", bd=1, relief=SUNKEN, anchor=W)
# status_bar.pack(side=BOTTOM, fill=X)

# -------------------
# Drawing
# -------------------

canvas = Canvas(root, width=200, height=100)
canvas.pack()

canvas.create_line(0, 0, 200, 150, fill="red")
canvas.create_image(0, 0)

root.mainloop()
