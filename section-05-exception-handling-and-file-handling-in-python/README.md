# Section 5: Exception Handling & File Handling In Python

## Overview:
- Error and exception in Python
- Exception handling
- File handling

### Exception handling
- **Try** and **Except**
- **Finally Block**

### File handling
 >**r** : read only, default mode, pointer at beginning of the file
 
 >**rb** : read only in binary, default mode, pointer at beginning of the file
 
 >**r+** : read + write , pointer at beginning of the file
 
 >**rb+** : read + write in binary format, pointer at beginning of the file


 >**w** : write only, overwrite if file exist or create new if file is not
 
 >**wb** : write only in binary mode, overwrite if file exist or create new if file is not
 
 >**a** : append the file, pointer at the end of the file

## Coding Challenge part 5

## Coding Challenge part 6