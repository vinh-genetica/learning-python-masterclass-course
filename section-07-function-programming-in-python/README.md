# Section 7:  Functional Programming In Python

## Overview:
- Brief about Functional programming
- **Lambdas** in python
- **Map** and **Filters**
    - Lambdas often using along with **Map**, **Filters** and **Reduce**
- **Generator** in python
    - Save memory
    
### Reduce in python
- Combine each 2 elements of the array
- [More info](https://www.geeksforgeeks.org/reduce-in-python/)
    
## Coding Challenge part 9

## Coding Challenge part 10

## Coding Challenge part 11