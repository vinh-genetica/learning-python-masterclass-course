def student_discount(price):
    return price * 0.9


def regular_discount(price):
    return price*.95


price = 1000

discounted_price = regular_discount(student_discount(price))

print(discounted_price)
