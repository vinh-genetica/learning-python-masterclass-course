from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Chrome()
# driver.get('https://www.google.com.vn/')

###################
# Automation searching using xpath
###################

# ele = driver.find_element_by_xpath('//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input')
# time.sleep(2)
# ele.clear()
# ele.send_keys('Python')
# ele.send_keys(Keys.RETURN)

###################
# Clicking links
###################

# ele = driver.find_element_by_link_text('Giới thiệu')
# time.sleep(2)
# ele.click()
# ele = driver.find_element_by_link_text('Sản phẩm')
# time.sleep(2)
# ele.click()

###################
# Refreshing Webpage
###################

# time.sleep(5)
# driver.refresh()

###################
# Using Forward and Backward Navigation Buttons
###################

# ele = driver.find_element_by_link_text('Giới thiệu')
# time.sleep(2)
# ele.click()
# ele = driver.find_element_by_link_text('Sản phẩm')
# time.sleep(2)
# ele.click()
#
# time.sleep(5)
# driver.back()
#
# time.sleep(5)
# driver.forward()

###################
# Scrolling and Getting the Current URL
###################

driver.get('https://en.wiktionary.org/wiki/Wiktionary:Main_Page')

time.sleep(5)
ele = driver.find_element_by_tag_name('html')
ele.send_keys(Keys.END)

time.sleep(5)
ele.send_keys(Keys.HOME)

time.sleep(5)
url = driver.current_url
print(url)