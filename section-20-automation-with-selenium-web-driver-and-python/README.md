# Section 20: Automation With Selenium Web Driver and Python

## Overview:
- Introduction to Selenium
- Installing Selenium
- Selenium webdriver:
    - Opening a url
    - Automating google search using xpath
    - Clicking links
    - Refreshing a Webpage
    - Using Forward and Backward Navigation Buttons
    - Scrolling and Getting the Current URL
#### Opening url
```
from selenium import webdriver

driver = webdriver.Chrome()
driver.get('https://www.google.com/')
```  

#### Automating google search using xpath
```
...
from selenium.webdriver.common.keys import Keys
import time
...
...
ele = driver.find_element_by_xpath('//*[@id="tsf"]/div[2]/div[1]/div[1]/div/div[2]/input')
time.sleep(2)
ele.clear()
ele.send_keys('Python')
ele.send_keys(Keys.RETURN)
```     

#### Clicking links
```
...
ele = driver.find_element_by_link_text('Giới thiệu')
time.sleep(2)
ele.click()
ele = driver.find_element_by_link_text('Sản phẩm')
time.sleep(2)
ele.click()
```        

#### Refreshing a Webpage
```
...
time.sleep(5)
driver.refresh()
```

####  Using Forward and Backward Navigation Buttons
- Backward
```
time.sleep(5)
driver.back()
```
- Forward
```
time.sleep(5)
driver.forward()
```

#### Scrolling and Getting the Current URL
- Get *html* element
```
ele = driver.find_element_by_tag_name('html')
```
- Scroll down
```
ele.send_keys(Keys.END)
```
- Scroll up
```
ele.send_keys(Keys.HOME)
```
- Get current url
```
url = driver.current_url
```