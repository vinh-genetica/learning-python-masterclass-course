# SOLUTION FOR BOTH LEVEL 1 AND LEVEL

from tkinter import *
from tkinter import filedialog
import os

FILE_NAME = ''


def new_file():
    global FILE_NAME
    FILE_NAME = ''
    text.config(state=NORMAL)
    text.delete('1.0', END)
    submenu.entryconfig(2, state=NORMAL)


def open_file():
    global FILE_NAME
    FILE_NAME = filedialog.askopenfilename(title="Select file", filetypes=[("text files", "*.txt"), ])
    f = open(FILE_NAME, 'r', encoding='utf-8')
    file_text = f.read()
    text.config(state=NORMAL)
    text.delete('1.0', END)
    text.insert('1.0', file_text)
    f.close()
    submenu.entryconfig(2, state=NORMAL)
    submenu.entryconfig(3, state=NORMAL)
    submenu.entryconfig(4, state=NORMAL)


def save_file():
    global FILE_NAME
    if FILE_NAME != '':
        f = open(FILE_NAME, 'w')
        f.write(text.get('1.0', END))
        f.close()
    else:
        new_file_name = filedialog.asksaveasfilename(filetypes=[("text files", "*.txt"), ])
        if new_file_name:
            f = open(new_file_name + ".txt", 'w', encoding='utf-8')
            f.write(text.get('1.0', END))
            f.close()


def system_exit():
    raise SystemExit


def close_file():
    global FILE_NAME
    FILE_NAME = ''
    text.delete('1.0', END)
    text.config(state=DISABLED)
    submenu.entryconfig(2, state=DISABLED)
    submenu.entryconfig(3, state=DISABLED)
    submenu.entryconfig(4, state=DISABLED)


def delete_file():
    global FILE_NAME
    if FILE_NAME != '':
        os.remove(FILE_NAME)
    text.delete('1.0', END)
    text.config(state=DISABLED)
    submenu.entryconfig(2, state=DISABLED)
    submenu.entryconfig(3, state=DISABLED)
    submenu.entryconfig(4, state=DISABLED)


root = Tk()
root.title("Challenge 2: tax calculator")

my_menu = Menu(root)
root.config(menu=my_menu)

submenu = Menu(my_menu)

my_menu.add_cascade(label="File", menu=submenu)

submenu.add_command(label="New file", command=new_file)
submenu.add_command(label="Open file", command=open_file)
submenu.add_command(label="Save file", command=save_file, state=DISABLED)
submenu.add_command(label="Delete file", command=delete_file, state=DISABLED)

submenu.add_separator()

submenu.add_command(label="Close", command=close_file, state=DISABLED)
submenu.add_command(label="Exit", command=system_exit)

text = Text(root, width=70, height=30)
text.config(state=DISABLED)
text.pack()

status_bar = Label(root, text="", bd=1, relief=SUNKEN, anchor=W)
status_bar.pack(side=BOTTOM, fill=X)

root.mainloop()
