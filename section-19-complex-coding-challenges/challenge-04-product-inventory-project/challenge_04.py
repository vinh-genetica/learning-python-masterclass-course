import sqlite3


#########################
# START Create database
#########################

conn = sqlite3.connect('product.db')
cur = conn.cursor()

cur.execute('''CREATE TABLE PRODUCT ([id] INTEGER PRIMARY KEY,
             [productName] text, [price] integer, [quantity] integer)''')

conn.commit()

#########################
# END Create database
#########################

class DBContext:

    def __init__(self):
        self.conn = sqlite3.connect('product.db')
        self.cur = self.conn.cursor()

    def close_connection(self):
        if self.conn is not None:
            self.conn.close()

    def select_all(self):
        self.cur.execute('''SELECT * FROM PRODUCT''')
        rows = self.cur.fetchall()
        return rows

    def search_by_id(self, id):
        query = '''SELECT * FROM PRODUCT WHERE id=?'''
        self.cur.execute(query, (id,))

        return self.cur.fetchone()

    def add_new_product(self, product_name, price, quantity):
        query = '''INSERT INTO PRODUCT(productName,price,quantity) VALUES (?,?,?)'''
        self.cur.execute(query, (product_name, price, quantity))
        self.conn.commit()

    def delete_by_id(self, id):
        query = '''DELETE FROM PRODUCT WHERE id=?'''
        self.cur.execute(query, (id,))

        self.conn.commit()

    def update_exist_product(self, product_name, price, quantity, id):
        query = '''
        UPDATE PRODUCT 
        SET productName = ? , 
            price = ?,
            quantity = ?
        WHERE 
            id = ?    
        '''
        self.cur.execute(query, (product_name, price, quantity, id))
        self.conn.commit()


def get_integer_input(message, min_input=None, max_input=None):
    while True:
        try:
            number = int(input(message))
            if check_number_in_range(number, min_input, max_input):
                return number
        except:
            print("Invalid input, please try again!")


def check_number_in_range(number, min_input=None, max_input=None):
    if min_input is not None:
        if number < min_input:
            print("Number is too small, min = " + str(min_input))
            return False
    if max_input is not None:
        if number > max_input:
            print("Number is too large, max = " + str(max_input))
            return False
    return True


def print_product_info(product):
    print("ID: " + str(product[0]))
    print("Product name: " + product[1])
    print("Product price: " + str(product[2]))
    print("Product quantity: " + str(product[3]))
    print("")


db = DBContext()
while True:
    print("1.Select all")
    print("2.Search by ID")
    print("3.Add new product")
    print("4.Delete by id")
    print("5 Edit product ")
    print("6.Calculate total by ID")
    print("7.Calculate total of all product")
    print("8.Exit")

    choice = get_integer_input("WHat do you want? ", 1, 7)

    if choice == 1:
        for product in db.select_all():
            print_product_info(product)

    if choice == 2:
        id = get_integer_input("Please enter search ID: ")
        product = db.search_by_id(id)
        if product is not None:
            print_product_info(product)
        else:
            print("No product found!")

    if choice == 3:
        name = input("Enter product name: ")
        price = get_integer_input("Please enter product price: ", 0)
        quantity = get_integer_input("Please enter product quantity: ", 0)
        db.add_new_product(name, price, quantity)

    if choice == 4:
        id = get_integer_input("Please enter product ID to delete: ")
        db.delete_by_id(id)

    if choice == 5:
        id = get_integer_input("Please enter product ID to edit: ")
        product = db.search_by_id(id)
        if product is not None:
            print_product_info(product)
        else:
            print("No product found!")
        print("Please enter new info of the product:")
        name = input("Enter product name: ")
        price = get_integer_input("Please enter product price: ", 0)
        quantity = get_integer_input("Please enter product quantity: ", 0)
        db.update_exist_product(name, price, quantity, id)

    if choice == 6:
        id = get_integer_input("Please enter product ID to edit: ")
        product = db.search_by_id(id)
        print_product_info(product)
        print("Total of product: " + str(product[2] * product[3]))

    if choice == 7:
        total = 0
        for product in db.select_all():
            total += product[2] * product[3]
        print("Total of all product: " + str(total))

    if choice == 8:
        break

    cont = input("Continue (y/n)? Press any key to continue!")
    if cont == 'n':
        break
