from urllib.request import urlopen
from bs4 import BeautifulSoup


class QuoteModel:
    def __init__(self, quote, author, tags):
        self.quote = quote
        self.author = author
        self.tags = tags


def crawling(url):
    global quote_list, page_counter_flag, PAGE_URL
    crawling_url = PAGE_URL + url
    print(crawling_url)
    if page_counter_flag == 20:
        return

    html_string = ''
    response = urlopen(crawling_url)
    if 'text/html' in response.getheader('Content-Type'):
        html_bytes = response.read()
        html_string = html_bytes.decode("utf-8")

    soup = BeautifulSoup(html_string, "html.parser")

    for div in soup.findAll('div', {'class': 'quote'}):

        quote = div.find('span', {'class': 'text'})

        author = ''
        for span in div.findAll('span'):
            author = span.find('small', {'class': 'author'})

        tags = []
        tag_div = div.find('div', {'class': 'tags'})
        for tag in div.findAll('a', {'class': 'tag'}):
            tags.append(tag.text)

        quote_list.append(QuoteModel(quote.text, author.text, tags))

    page_counter_flag += 1
    next_li = soup.find('li', {'class': 'next'})
    if next_li is not None:
        next_url = next_li.find('a', href=True)['href']
        crawling(next_url)


quote_list = []
page_counter_flag = 0
PAGE_URL = "http://quotes.toscrape.com"
crawling('')

f = open("crawled.txt", "w", encoding='utf-8')
for quote_model in quote_list:
    tags_str = ''
    for tag in quote_model.tags:
        tags_str = tags_str + tag + "|"
    # print(quote_model.quote)
    # print(quote_model.author)
    # print(tags_str)
    # print()

    f.write(quote_model.quote + "|" + quote_model.author + "|" + tags_str + "\n")

f.close()
