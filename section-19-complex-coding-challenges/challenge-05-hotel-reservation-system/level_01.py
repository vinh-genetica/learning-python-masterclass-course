from db_context import DBContext


def get_integer_input(message, min_input=None, max_input=None):
    while True:
        try:
            number = int(input(message))
            if check_number_in_range(number, min_input, max_input):
                return number
        except:
            print("Invalid input, please try again!")


def check_number_in_range(number, min_input=None, max_input=None):
    if min_input is not None:
        if number < min_input:
            print("Number is too small, min = " + str(min_input))
            return False
    if max_input is not None:
        if number > max_input:
            print("Number is too large, max = " + str(max_input))
            return False
    return True


def print_room_info(room):
    print("ID: " + str(room[0]))
    print("Room_no: " + room[1])
    print("Floor: " + str(room[2]))
    print("Room type: " + ("penthouse" if room[3] else "Normal"))
    print("Is available: " + ("YES" if room[4] else "NO"))
    print()


db = DBContext()
while True:
    print("1.Get all room")
    print("2.Get all room available")
    print("3.Get all room unavailable")
    print("4.Get all penthouse")
    print("5.Get all penthouse available")
    print("6.Get all penthouse unavailable")
    print("7.Get all normal room")
    print("8.Get all normal room available")
    print("9.Get all normal room unavailable")
    print("0.Exit")

    choice = get_integer_input("WHat do you want? ", 1, 8)

    if choice == 1:
        for room in db.get_all():
            print_room_info(room)

    if choice == 2:
        for room in db.get_available():
            print_room_info(room)

    if choice == 3:
        for room in db.get_unavailable():
            print_room_info(room)

    if choice == 4:
        for room in db.get_all_penthouse():
            print_room_info(room)

    if choice == 5:
        for room in db.all_penthouse_available():
            print_room_info(room)

    if choice == 6:
        for room in db.all_penthouse_unavailable():
            print_room_info(room)

    if choice == 7:
        for room in db.get_all_normal_room():
            print_room_info(room)

    if choice == 8:
        for room in db.all_normal_room_available():
            print_room_info(room)

    if choice == 9:
        for room in db.all_normal_room_unavailable():
            print_room_info(room)

    if choice == 0:
        break

    cont = input("Continue (y/n)? Press any key to continue!")
    if cont == 'n':
        break
