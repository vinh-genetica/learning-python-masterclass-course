import sqlite3


# #########################
# # START Create database
# #########################
#
# conn = sqlite3.connect('hotel.db')
# cur = conn.cursor()
#
# cur.execute('''CREATE TABLE ROOM ([id] INTEGER PRIMARY KEY, [room_no] text,
#               [floor] integer, [is_penthouse] bit, [is_available] bit)''')
#
# conn.commit()
#
# #########################
# # END Create database
# #########################
#
# #########################
# # START INSERT data to database
# #########################
#
# conn = sqlite3.connect('hotel.db')
# cur = conn.cursor()
#
# query = '''INSERT INTO ROOM(room_no,floor,is_penthouse,is_available) VALUES (?,?,?,?)'''
#
# data = (["L101", 1, 1, 0], ["R101", 1, 0, 1], ["L102", 1, 0, 1],
#         ["L201", 2, 0, 0], ["R201", 2, 0, 1],
#         ["L301", 3, 0, 1], ["R301", 3, 0, 0], ["L302", 3, 1, 0], ["R302", 3, 1, 1],
#         ["L401", 4, 1, 1], ["R401", 4, 1, 1], ["L402", 4, 0, 0],
#         )
# for d in data:
#     cur.execute(query, (d[0], d[1], d[2],d[3]))
#
# conn.commit()


#########################
# END INSERT data1

#########################

class DBContext:

    def __init__(self):
        self.conn = sqlite3.connect('hotel.db')
        self.cur = self.conn.cursor()

    def close_connection(self):
        if self.conn is not None:
            self.conn.close()

    def get_all(self):
        self.cur.execute('''SELECT * FROM ROOM''')
        rows = self.cur.fetchall()
        return rows

    def get_available(self):
        self.cur.execute('''SELECT * FROM ROOM where is_available = TRUE ''')
        rows = self.cur.fetchall()
        return rows

    def get_unavailable(self):
        self.cur.execute('''SELECT * FROM ROOM where is_available = FALSE ''')
        rows = self.cur.fetchall()
        return rows

    def get_all_penthouse(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = True''')
        rows = self.cur.fetchall()
        return rows

    def all_penthouse_available(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = True and is_available = TRUE ''')
        rows = self.cur.fetchall()
        return rows

    def all_penthouse_unavailable(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = True and is_available = FALSE''')
        rows = self.cur.fetchall()
        return rows

    def get_all_normal_room(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = FALSE ''')
        rows = self.cur.fetchall()
        return rows

    def all_normal_room_available(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = FALSE and is_available = TRUE ''')
        rows = self.cur.fetchall()
        return rows

    def all_normal_room_unavailable(self):
        self.cur.execute('''SELECT * FROM ROOM where is_penthouse = FALSE and is_available = FALSE''')
        rows = self.cur.fetchall()
        return rows
