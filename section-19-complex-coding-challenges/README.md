# Section 19: Complex Coding Challenges

## Overview:
- Challenge 1: Tile contractor app
- Challenge 2: TAX calculator
- Challenge 3: Text editor
- Challenge 4: Product inventory project
- Challenge 5: Hotel Reservation System
- Challenge 6: Build a scientific calculator using Tkinter
- Challenge 7: Build a global clock which shows current local timings across the globe
- Challenge 8: Real time weather app
- Challenge 9: Web Crawler

### Challenge 1: Tile contractor app
#### Level 1:
- Create console app to accept input and print calculated output
- Validate input
- Result:
    ![review ss_01](images/ss_01.png)

#### Level 2: 
- Using **tkinter** to create GUI app 
- Validate input
- Accept values in different units
- Validate input
- Result:
    - Accept values in different units 
    
        ![review ss_02](images/ss_02.png)
        
    - Validate length 
        
        ![review ss_03](images/ss_03.png)
    
    - Validate discount
    
        ![review ss_04](images/ss_04.png)
    
    - Calculate and print result
    
        ![review ss_05](images/ss_05.png)
        
### Challenge 2: TAX calculator
#### Level 1:
- Create console app accept *age* and *income* input
- Calculate tax and return
- Result:

    ![review ss_06](images/ss_06.png)

#### Level 2:
- Create GUI to calculate tax
- Add additional field *tax category*
- Result

    ![review ss_07](images/ss_07.png)

### Challenge 3: Text editor
- Creat desktop app that can 
    - Open exist txt file
    - Save file
    - delete file
- Result:

    ![review ss_08](images/ss_08.png)
    
#### Challenge 4: Product inventory project
- Create console app that connect to database to manage product inventory
- Result: The app has following function
    - Get list of all product
        
        ![review ss_09](images/ss_09.png)   
    
    - Search by id
    
        ![review ss_10](images/ss_10.png)   
    
    - Add new product    
    
        ![review ss_11](images/ss_11.png)   

    - Delete product by ID
    
        ![review ss_12](images/ss_12.png)   

    - Edit exist product
    
        ![review ss_13](images/ss_13.png)   
    
    - Calculate total price of a product
    
        ![review ss_14](images/ss_14.png)   
    
    - Calculate total price of all product
    
        ![review ss_15](images/ss_15.png)   

#### Challenge 5: Hotel Reservation System   
- Create a application help manage hotel room
- Result

     ![review ss_22](images/ss_22.png)   

#### Challenge 6: Build a scientific calculator using Tkinter    
- Create scientific calculator 
- Result 

    ![review ss_16](images/ss_16.png)   

#### Challenge 7: Build a global clock which shows current local timings across the globe
- Create a simple clock using console
- Result

    ![review ss_17](images/ss_17.png)  
    
#### Challenge 8: Real time weather app
- Using third-party api, build simple weather web application
- User should be able to choose from different locations.
- Result  

    ![review ss_18](images/ss_18.png)  
    
    ![review ss_19](images/ss_19.png)  
    
#### Challenge 9: Web Crawler
- Crawling all quote of [This website](http://quotes.toscrape.com/)
- Result

   ![review ss_20](images/ss_20.png)  
   
   ![review ss_21](images/ss_21.png)   