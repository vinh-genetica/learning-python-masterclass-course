from django.shortcuts import render
import requests
import json
from datetime import datetime

week = ['Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday']


class Weather:
    def __init__(self, weather_main, temp, time, humidity):
        self.weather_main = weather_main
        self.temp = temp
        self.time = time
        self.weekday = week[(datetime.strptime(time, "%Y-%m-%d %H:%M:%S")).weekday()]
        self.humidity = humidity
        image_url = ''
        if weather_main.lower().find("rain") > -1:
            image_url = 'images/rain.png'
        if weather_main.lower().find("sun") > -1:
            image_url = 'images/sunn.png'
        if weather_main.lower().find("cloud") > -1:
            image_url = 'images/cloudy.png'
        self.image_url = image_url


def get_json(city_name):
    API_KEY = "7e1268e2c188e78878f309ed53b83a3c"
    url = "http://api.openweathermap.org/data/2.5/" \
          "forecast?q={city name}" \
          "&appid={your api key}"
    url = url.replace("{your api key}", API_KEY)
    url = url.replace("{city name}", city_name)
    response = requests.request("GET", url).content
    myson = json.loads(response)
    data = []
    for idx, d in enumerate(myson['list']):
        if (idx + 1) in [2, 6, 10, 12]:
            data.append(Weather(d['weather'][0]['main'],
                                d['main']['temp'],
                                d['dt_txt'],
                                d['main']['humidity']))
    return data


# data = get_json('Hanoi')
# for d in data:
#     print('weather_main = ' + d.weather_main)
#     print('temp = ' + str(d.temp))
#     print('time = ' + str(d.time))
#     print('humidity = ' + str(d.humidity))
#     print()

def index(request, city_name='Hanoi'):
    data = get_json(city_name)
    context = {
        'data': data,
        'city_name': city_name.replace("%20"," "),
    }
    return render(request, "index.html", context)
