from django.contrib import admin
from django.urls import path
from .views import index

app_name = "weatherapp"
urlpatterns = [
    path('<str:city_name>', index, name='index'),
    path('', index, name='index'),
]
