def get_float_input(message, min_input=None, max_input=None):
    while True:
        try:
            number = float(input(message))
            if min_input is not None:
                if number < min_input:
                    print("Number is too small, min = " + str(min_input))
                    continue
            if max_input is not None:
                if number > max_input:
                    print("Number is too large, max = " + str(max_input))
                    continue
            return number
        except:
            print("Invalid input, please try again!")


def float_to_string(num):
    num = int(num) if num == int(num) else num
    return str(num)


PRICE_PER_SQUARE_FEET = 20

length = get_float_input("Please enter length:", 0)
breadth = get_float_input("Please enter breadth:", 0)

discount = 0
while True:
    is_discount = input("Will the charges be discounted(y/n) ")
    if (is_discount != 'y') and (is_discount != 'n'):
        print("invalid input! Please try again!")
        continue

    if is_discount == 'y':
        discount = get_float_input("Please enter discount percent:", 0, 100)
        break
    else:
        break

area_covered = length * breadth
total_pre_discount_amount = area_covered * PRICE_PER_SQUARE_FEET
total_discounted = total_pre_discount_amount * discount / 100
total_amount = total_pre_discount_amount - total_discounted
print("Total area covered: " + float_to_string(area_covered))
print("Total discount: " + float_to_string(total_discounted))
print("Total amount customer has to pay: " + float_to_string(total_amount))
