from tkinter import *

PRICE_PER_SQUARE_FEET = 20

root = Tk()
root.title("Challenge 1: Tile contractor app")

canvas = Canvas(root, height=350, width=400)
canvas.pack()

frame = Frame()
frame.place(relx=0.1, rely=0.02, relwidth=0.8, relheight=0.8)


def foot_to_meters(num):
    return num * 0.3048


def inch_to_meters(num):
    return num * 0.0254


# Row=7
area_covered_label = Label(frame, text="", fg='green')
area_covered_label.grid(row=7, column=0, columnspan=3, sticky="W")

# Row=8
total_discounted_label = Label(frame, text="", fg='green')
total_discounted_label.grid(row=8, column=0, columnspan=3, sticky="W")

# Row=9
total_amount_label = Label(frame, text="", fg='green')
total_amount_label.grid(row=9, column=0, columnspan=3, sticky="W")


def print_results(area_covered, total_discounted, total_amount):
    area_covered_label['text'] = "AREA COVERED: " + str(area_covered)
    total_discounted_label['text'] = "DISCOUNTED: " + str(total_discounted)
    total_amount_label['text'] = "Customer has to pay: " + str(total_amount)


def clear_error_message():
    discount_error_label['text'] = ""
    length_error_label['text'] = ""
    breadth_error_label['text'] = ""


def calculate():
    clear_error_message()
    try:
        length = float(length_entry.get())

        if length < 0:
            length_error_label['text'] = "Length cannot less than 0"
            return

        if tk_var1.get() == "feet":
            length = foot_to_meters(length)
        if tk_var1.get() == "inches":
            length = inch_to_meters(length)

    except Exception as ex:
        length_error_label['text'] = repr(ex)
        return

    try:
        breadth = float(breadth_entry.get())

        if breadth < 0:
            breadth_error_label['text'] = "Breadth cannot less than 0"
            return

        if tk_var2.get() == "feet":
            length = foot_to_meters(length)
        if tk_var2.get() == "inches":
            length = inch_to_meters(length)

    except Exception as ex:
        breadth_error_label['text'] = repr(ex)
        return

    try:
        discount = float(discount_entry.get())
        if discount < 0:
            discount_error_label['text'] = "Discount cannot less than 0"
            return

        if discount > 100:
            discount_error_label['text'] = "Discount cannot greater than 100"
            return

    except Exception as ex:
        discount_error_label['text'] = repr(ex)
        return

    area_covered = length * breadth
    total_pre_discount_amount = area_covered * PRICE_PER_SQUARE_FEET
    total_discounted = total_pre_discount_amount * discount / 100
    total_amount = total_pre_discount_amount - total_discounted

    print_results(area_covered, total_discounted, total_amount)


# Row=0
Label(frame, text="Length").grid(row=0, column=0, padx=10, pady=10)
length_entry = Entry(frame)
length_entry.grid(row=0, column=1, padx=10, pady=10)
length_entry.insert(END, '0')
tk_var1 = StringVar(root)
choices1 = {'meter', 'feet', 'inches'}
tk_var1.set('meter')
length_popupMenu = OptionMenu(frame, tk_var1, *choices1)
length_popupMenu.grid(row=0, column=2, padx=10, pady=10)

# Row=1
length_error_label = Label(frame, text="", fg='red')
length_error_label.grid(row=1, column=0, columnspan=3, sticky="W")

# Row=2
Label(frame, text="Breadth").grid(row=2, column=0)
breadth_entry = Entry(frame)
breadth_entry.grid(row=2, column=1)
breadth_entry.insert(END, '0')
tk_var2 = StringVar(root)
choices2 = {'meter', 'feet', 'inches'}
tk_var2.set('meter')
breadth_popupMenu = OptionMenu(frame, tk_var2, *choices2)
breadth_popupMenu.grid(row=2, column=2, padx=10, pady=10)

# Row=3
breadth_error_label = Label(frame, text="", fg='red')
breadth_error_label.grid(row=3, column=0, columnspan=3, sticky="W")

# Row=4
Label(frame, text="Discount").grid(row=4, column=0)
discount_entry = Entry(frame)
discount_entry.grid(row=4, column=1)
discount_entry.insert(END, '0')

# Row=5
discount_error_label = Label(frame, text="", fg='red')
discount_error_label.grid(row=5, column=0, columnspan=3, sticky="W")

# Row=6
Button(frame, text="Calculate", command=lambda: calculate()).grid(row=6, column=2, sticky=NSEW)
root.mainloop()
