from tkinter import *

switcher = {
    'Small Businesses': 3.5,
    "Company": 6,
    "Farmer": 1
}

root = Tk()
root.title("Challenge 2: tax calculator")

canvas = Canvas(root, height=350, width=560)
canvas.pack()

frame = Frame()
frame.place(relx=0.1, rely=0.02, relwidth=0.8, relheight=0.8)


def print_results(income_tax, category_tax, total_tax):
    income_tax_label['text'] = "Income tax: " + str(income_tax)
    category_tax_label['text'] = "Taxpayers's category tax: " + str(category_tax)
    total_tax_label['text'] = "Total TAX you must pay: " + str(total_tax)


def clear_error_message():
    age_error_label['text'] = ""
    income_error_label['text'] = ""


def calculate():
    clear_error_message()
    try:
        age = int(age_entry.get())

        if age < 0:
            age_error_label['text'] = "Age cannot less than 0"
            return

    except Exception as ex:
        age_error_label['text'] = repr(ex)
        return

    try:
        income = float(income_entry.get())

        if income < 0:
            income_error_label['text'] = "Income cannot less than 0"
            return

    except Exception as ex:
        income_error_label['text'] = repr(ex)
        return

    total_tax = 0
    income_tax = 0
    category_tax = 0
    if income < 20001:
        total_tax = 0
    else:
        if age < 60:
            if income in range(20001, 50001):
                income_tax = income * 0.20
            if income in range(50001, 100001):
                income_tax = income * 0.30
            if income > 100000:
                income_tax = income * 0.40

            taxpayers_category = tk_var.get()
            category_tax = switcher.get(taxpayers_category, 0) * income
            total_tax = income_tax + category_tax
        else:
            if income in range(20001, 50001):
                income_tax = income * 0.10
            if income in range(50001, 100001):
                income_tax = income * 0.20
            if income > 100000:
                income_tax = income * 0.30
            total_tax = income_tax

    print_results(income_tax, category_tax, total_tax)


# Row=0
Label(frame, text="Age").grid(row=0, column=0, padx=10, pady=10)
age_entry = Entry(frame)
age_entry.grid(row=0, column=1, padx=10, pady=10)
age_entry.insert(END, '0')

# Row=1
age_error_label = Label(frame, text="", fg='red')
age_error_label.grid(row=1, column=0, columnspan=3, sticky="W")

# Row=2
Label(frame, text="Income").grid(row=2, column=0)
income_entry = Entry(frame)
income_entry.grid(row=2, column=1)
income_entry.insert(END, '0')

# Row=3
income_error_label = Label(frame, text="", fg='red')
income_error_label.grid(row=3, column=0, columnspan=3, sticky="W")

# Row=4
Label(frame, text="Taxpayers category").grid(row=4, column=0, padx=10, pady=10)
tk_var = StringVar(root)
choices = {'None', 'Small Businesses', 'Company', 'Farmer'}
tk_var.set('None')
category_popupMenu = OptionMenu(frame, tk_var, *choices)
category_popupMenu.grid(row=4, column=2, padx=10, pady=10)

# Row=6
income_tax_label = Label(frame, text="", fg='green')
income_tax_label.grid(row=6, column=0, columnspan=3, sticky="W")

# Row=7
category_tax_label = Label(frame, text="", fg='green')
category_tax_label.grid(row=7, column=0, columnspan=3, sticky="W")

# Row=8
total_tax_label = Label(frame, text="", fg='green')
total_tax_label.grid(row=8, column=0, columnspan=3, sticky="W")

# Row=5
Button(frame, text="Calculate", command=lambda: calculate()).grid(row=5, column=2, sticky=NSEW)
root.mainloop()
