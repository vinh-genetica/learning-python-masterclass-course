def get_integer_input(message, min_input=None, max_input=None):
    while True:
        try:
            number = int(input(message))
            if check_number_in_range(number, min_input, max_input):
                return number
        except:
            print("Invalid input, please try again!")


def get_float_input(message, min_input=None, max_input=None):
    while True:
        try:
            number = float(input(message))
            if check_number_in_range(number, min_input, max_input):
                return number
        except:
            print("Invalid input, please try again!")


def check_number_in_range(number, min_input=None, max_input=None):
    if min_input is not None:
        if number < min_input:
            print("Number is too small, min = " + str(min_input))
            return False
    if max_input is not None:
        if number > max_input:
            print("Number is too large, max = " + str(max_input))
            return False
    return True


age = get_integer_input("Please enter age:", 0)
income = get_float_input("Please enter income:", 0)

tax = None
if income < 20001:
    tax = 0
else:
    if age < 60:
        if income in range(20001, 50001):
            tax = income * 0.20
        if income in range(50001, 100001):
            tax = income * 0.30
        if income > 100000:
            tax = income * 0.40
    else:
        if income in range(20001, 50001):
            tax = income * 0.10
        if income in range(50001, 100001):
            tax = income * 0.20
        if income > 100000:
            tax = income * 0.30

print("Tax you must paid: " + str(tax) + "$")
