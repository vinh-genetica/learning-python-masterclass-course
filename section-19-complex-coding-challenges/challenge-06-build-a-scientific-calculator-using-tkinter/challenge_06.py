from tkinter import *
import parser
import itertools
import math
import numpy

root = Tk()
root.title("calculator")

root.grid_columnconfigure(0, weight=1, uniform="fred")

display = Entry(root)
display.grid(row=0, columnspan=8, sticky=W + E)

is_rad_flag = True
is_invoke = True


def create_button(text, command, grid_row, grid_col):
    button = Button(root, text=text, command=command, width=5, height=3)
    button.grid(row=grid_row, column=grid_col)
    return button


# Append var to the
def get_var(var):
    entry_string = display.get()
    if (entry_string == "Error"):
        clear_all()

    display.insert(len(entry_string), var)


# Clear all
def clear_all():
    display.delete(0, len(display.get()))


# Delete last char of the string
def undo():
    display.delete(0, 1)


def fac(num):
    return math.factorial(num)


def calculate():
    entry_string = display.get()
    try:
        a = parser.expr(entry_string).compile()
        result = eval(a)
        clear_all()
        display.insert(0, result)
    except Exception:
        clear_all()
        display.insert(0, "Error")


def e():
    return math.e


def sin(num):
    return math.sin(math.radians(num) if is_rad_flag else num)


def cos(num):
    return math.cos(math.radians(num) if is_rad_flag else num)


def tan(num):
    return math.tan(math.radians(num) if is_rad_flag else num)


def arcsin(num):
    return numpy.arcsin(math.radians(num) if is_rad_flag else num)


def arccos(num):
    return float(numpy.arccos(math.radians(num) if is_rad_flag else num))


def arctan(num):
    return float(numpy.arctan(math.radians(num) if is_rad_flag else num))


def switch_rad_deg():
    global is_rad_flag
    is_rad_flag = not is_rad_flag
    rad_btn.config(state=NORMAL if is_rad_flag else DISABLED)
    deg_btn.config(state=DISABLED if is_rad_flag else NORMAL)


def invoke():
    global is_invoke
    is_invoke = not is_invoke
    sin_btn.config(text=("sin^-1" if is_invoke else "sin"))
    cos_btn.config(text=("cos^-1" if is_invoke else "cos"))
    tan_btn.config(text=("tan^-1" if is_invoke else "tan"))


# Create number button
for x, y in itertools.product(range(1, 4), range(0, 3)):
    create_button(str(x + y * 3), lambda num=(x + y * 3): get_var(str(num)), y + 1, x - 1)
    # Button(root, text=str(x + (y * 3)), command=lambda num=(x + y * 3): get_numbers(num)) \
    #     .grid(row=y + 1, column=x - 1, sticky=NSEW)

# Create other button
create_button("AC", lambda: clear_all(), 4, 0)
create_button("0", lambda: get_var("0"), 4, 1)
create_button(".", lambda: get_var("."), 4, 2)

# Create operators button
create_button("+", lambda: get_var("+"), 1, 3)
create_button("-", lambda: get_var("-"), 2, 3)
create_button("*", lambda: get_var("*"), 3, 3)
create_button("/", lambda: get_var("/"), 4, 3)

create_button("PI", lambda: get_var("3.14"), 1, 4)
create_button("%", lambda: get_var("%"), 2, 4)
create_button("(", lambda: get_var("("), 3, 4)
create_button("EXP", lambda: get_var("**"), 4, 4)

create_button("DEL", lambda: undo(), 1, 5)
create_button("x!", lambda: get_var("fac("), 2, 5)
create_button(")", lambda: get_var(")"), 3, 5)
create_button("e", lambda: get_var("*e()"), 4, 5)

rad_btn = create_button("Rad", lambda: switch_rad_deg(), 1, 6)
sin_btn = create_button("sin", lambda: get_var(("arcsin(" if is_invoke else "sin(")), 2, 6)
cos_btn = create_button("cos", lambda: get_var(("arccos(" if is_invoke else "cos(")), 3, 6)
tan_btn = create_button("tan", lambda: get_var(("arctan(" if is_invoke else "tan(")), 4, 6)

deg_btn = create_button("Deg", lambda: switch_rad_deg(), 1, 7)
create_button("Inv", lambda: invoke(), 2, 7)
create_button("π", lambda: get_var("math.pi"), 3, 7)
create_button("=", lambda: calculate(), 4, 7)

switch_rad_deg()
root.mainloop()
