import os
import pytz
from datetime import datetime, timezone
import threading

# Windows
clear = lambda: os.system('cls')

# # Linux
# clear = lambda: os.system('clear')

zone_list = {'US/Hawaii', 'Hongkong', 'Greenwich', 'Mexico/BajaNorte', 'Pacific/Niue'}


def update_clock():
    threading.Timer(1.0, update_clock).start()
    clear()
    for zone in zone_list:
        zone_time = datetime.now(pytz.timezone(zone))
        print(zone_time.strftime(str(zone) + ': %m/%d/%Y, %H:%M:%S'))


update_clock()
