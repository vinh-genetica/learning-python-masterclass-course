# Section 14:  Make Web Applications In Python Using Django

## Overview:
- Installing `Django 1.11.2` on Windows
- `Django 1.11.2`
    - Creating new app 
    - Creating views
    - RegEx URls
    - Database
        - Applying Migrations
        - Creating table
        - Adding, Filtering data to table using python console
        - Image field
    - Admin panel
    - Connecting to database
    - Create template
    - Rendering template
    - Raising 404 error
    - Removing hardcoded url
    - Namespace in Django
    - Using static file
    - Base template/ layout
    - Genetic view