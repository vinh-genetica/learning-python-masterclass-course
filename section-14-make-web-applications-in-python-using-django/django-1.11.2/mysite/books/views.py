from django.http import HttpResponse, Http404
from django.template import loader
from .models import Book
from django.shortcuts import render


def index(request):
    all_books = Book.objects.all()
    context = {
        'all_books': all_books
    }
    return render(request, "books/index.html", context)


def detail(request, id):
    try:
        book = Book.objects.get(id=id)
    except Book.DoesNotExist:
        raise Http404("this book does not exist!")
    return render(request, "books/detail.html", {'book': book})
