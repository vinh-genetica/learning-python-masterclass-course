"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views,generic_views

app_name = "books"
urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<id>[0-9]+)/$', views.detail, name="detail"),
    url(r'^$', views.index, name="add"),

    url(r'^generic$', generic_views.IndexView.as_view(), name='generic_index'),
    url(r'^generic/(?P<pk>[0-9]+)/$', generic_views.DetailView.as_view(), name='generic_detail'),
]

