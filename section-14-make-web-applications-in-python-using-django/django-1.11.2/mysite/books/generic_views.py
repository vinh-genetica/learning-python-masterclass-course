from django.views import generic
from .models import *


class IndexView(generic.ListView):
    template_name = "books/generic/index.html"

    def get_queryset(self):
        return Book.objects.all()


class DetailView(generic.DetailView):
    model = Book
    template_name = "books/generic/detail.html"
