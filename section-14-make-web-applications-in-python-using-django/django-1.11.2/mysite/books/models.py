from django.db import models


# python manage.py makemigrations
# python manage.py sqlmigrate books 0001
# python manage.py migrate
# python manage.py flush -> clear DB

class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Book(models.Model):
    def __str__(self):
        return "[" + self.name + "]-[" + self.author + "]"

    name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    price = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
