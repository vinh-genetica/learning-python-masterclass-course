import psycopg2
from tkinter import *
import tkinter
import itertools


class Student:
    def __init__(self, id, name, age, address):
        self.id = id
        self.name = name
        self.age = age
        self.address = address


def insert(name, age, address):
    conn = psycopg2.connect(dbname="section12", user="stuncb", port='5432', password="root")
    cur = conn.cursor()
    query = '''INSERT INTO students(name,age,address) VALUES (%s,%s,%s)'''
    cur.execute(query, (name, age, address))

    conn.commit()
    conn.close()


def delete(id):
    conn = psycopg2.connect(dbname="section12", user="stuncb", port='5432', password="root")
    cur = conn.cursor()
    query = '''DELETE FROM students WHERE id=%s'''
    cur.execute(query, (id,))

    conn.commit()
    conn.close()


def search(id):
    conn = psycopg2.connect(dbname="section12", user="stuncb", port='5432', password="root")
    cur = conn.cursor()
    query = '''SELECT * FROM students WHERE id=%s'''
    cur.execute(query, (id,))

    record = cur.fetchone()

    conn.commit()
    conn.close()

    name_entry.delete(0, len(name_entry.get()))
    name_entry.insert(0, record[0])

    age_entry.delete(0, len(age_entry.get()))
    age_entry.insert(0, record[1])

    address_entry.delete(0, len(name_entry.get()))
    address_entry.insert(0, record[2])


root = Tk()
root.title("Simple Database Application")

canvas = Canvas(root, height=240, width=200)
canvas.pack()

frame = Frame()
frame.place(relx=0.1, rely=0.02, relwidth=0.8, relheight=0.8)

Label(frame, text="Name").grid(row=0, column=0)
name_entry = Entry(frame)
name_entry.grid(row=0, column=1)

Label(frame, text="Age").grid(row=1, column=0)
age_entry = Entry(frame)
age_entry.grid(row=1, column=1)

Label(frame, text="Address").grid(row=2, column=0)
address_entry = Entry(frame)
address_entry.grid(row=2, column=1)

Button(frame, text="Add",
       command=lambda: insert(name_entry.get(), age_entry.get(), address_entry.get())) \
    .grid(row=4, column=1, sticky=NSEW)

Label(frame, text="SEARCH/DELETE HERE").grid(row=5, columnspan=2, column=0)

Label(frame, text="ID").grid(row=6, column=0)
id_entry = Entry(frame)
id_entry.grid(row=6, column=1)

Button(frame, text="Seach",
       command=lambda: search(id_entry.get())) \
    .grid(row=7, column=1, sticky=NSEW)

Button(frame, text="Delete",
       command=lambda: delete(id_entry.get())) \
    .grid(row=8, column=1, sticky=NSEW)

root.mainloop()
