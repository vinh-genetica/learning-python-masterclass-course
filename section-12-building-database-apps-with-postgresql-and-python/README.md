# Section 12:  Building Database Apps With PostgreSQL & Python

## Overview:
- Database introduction
- PostgreSQL
    - How to install
    - Command: 
        - Create new database
        - Drop database
        - Create table
        - Insert record to a table
        - Query data 
- Install Virtualenv and Psycopg2
    - Connecting to database using Python code
        - Create table 
        - Insert record 
        - Query data      
- Building simple database app with Tkinter and PostgreSQL
    - Preview

        ![Preview databe_application](demo.png)