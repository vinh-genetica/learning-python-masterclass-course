class computer:
    def __init__(self, resolution="", producer=""):
        self.resolution = resolution
        self.producer = producer

    def get_specs(self):
        self.resolution = input("Please enter resolution: ")
        self.producer = input("Please enter producer: ")

    def display_specs(self):
        print("resolution: " + self.resolution)
        print("producer: " + self.producer)


class PC(computer):
    __type = "Personal computer"

    def __init__(self, special=""):
        self.special = special

    def get_special_specs(self):
        self.special = input("Please enter special specs: ")

    def display_special(self):
        print("special: " + self.special)

    def display_all(self):
        print(self.__type)
        super().display_specs()
        self.display_special()


class laptop(computer):
    __type = "Laptop computer"

    def __init__(self, weight=""):
        self.weight = weight

    def get_weight_specs(self):
        self.weight = input("Please enter special specs: ")

    def display_weight(self):
        print("weight: " + self.weight)

    def display_all(self):
        print(self.__type)
        super().display_specs()
        self.display_weight()


pc = PC()
pc.get_specs()
pc.get_special_specs()
pc.display_all()

lap = laptop()
lap.get_specs()
lap.get_weight_specs()
lap.display_all()
