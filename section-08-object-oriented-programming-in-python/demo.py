# ------------------
# Classes in python
# ------------------
class student:
    __hidden_variable = ""

    def __init__(self, firstname="", lastname="", age=0, phone_number=""):
        self.fullname = firstname + " " + lastname
        self.age = age
        self.phone_number = phone_number

    def change_phone_number(self, phone_number):
        self.phone_number = phone_number

    def get_data(self):
        self.fullname = input("Please enter student fullname: ")
        check_flag = False
        while not check_flag:
            try:
                self.age = int(input("Please enter student age: "))
            except ValueError:
                continue
            finally:
                check_flag = True

        self.phone_number = input("Please enter student phone number: ")

    def put_data(self):
        print(self.fullname + "|" + str(self.age) + "|" + self.phone_number)


class science_student(student):

    def science(self):
        print("This is science of student name " + self.fullname)


student1 = student("Vinh", "Luong Tuan ", 23)
print(student1.fullname + ",age = " + str(student1.age))

student1.phone_number = "0914666999"

print(student1.phone_number)

student2 = student()
student2.get_data()
student2.put_data()

# ------------------
# Inheritance in python
# ------------------

student3 = science_student()
student3.get_data()
student3.science()


# ------------------
# Multiple inheritance in python
# ------------------

class A:
    def method_a(self):
        print("this is method from A")


class B:
    def method_b(self):
        print("this is method from B")


class C(A, B):
    def method_c(self):
        print("this is method from C")


class_c = C()
class_c.method_a()
class_c.method_b()
class_c.method_c()


# ------------------
# Multi-level inheritance in python
# ------------------

class D(C):
    def method_d(self):
        print("this is method from D")


class_d = D()
class_d.method_a()
class_d.method_b()
class_d.method_c()
class_d.method_d()


# ------------------
# Recursion in python
# ------------------

def factorical(x):
    if x == 1:
        return 1
    else:
        return x * (factorical(x - 1))


print("5! = " + str(factorical(5)))
