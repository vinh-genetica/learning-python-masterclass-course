# Section 8:  Object Oriented Programming In Python

## Overview:
- What is OOP programming, Classes and Object
    - Unlike C#, Python class does not allow multiple constructor
- Inheritance , multiple inheritance, multi-level inheritance in Python
- Recursion in Python
- Sets and sets operation
- Itertools
    - Powerful tools for looping function
- Operator overload
- Data hiding/encapsulation in Python
    
## Coding Challenge part 12

## Coding Challenge part 13