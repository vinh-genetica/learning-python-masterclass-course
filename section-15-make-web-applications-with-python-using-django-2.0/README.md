# Section 15:  Make Web Applications With Python Using Django 2.0

## Overview:
- Installing `Django` and setting up Django project on Windows
- `Django`
    - Creating new app 
    - Creating views
    - Database
        - Applying Migrations
        - Creating table
        - Adding, Filtering data to table using python console
        - Image field
        - Create superuser
    - Admin panel
    - Connecting to database
    - Create template
    - Rendering template
    - Raising 404 error
    - Removing hardcoded url
    - Using namespace
    - Using static file
    - Base template/ layout
