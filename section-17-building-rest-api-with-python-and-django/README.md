# Section 17: Building REST API's with Python and Django

## Overview:
- API Introduction
- Building simple REST API 

### Django REST Framework
- Install 
```
pip install djangorestframework
```

### Building basic REST API:
- Create models
    ```
    class Task(models.Model):
        name = models.CharField(max_length=1000)
        detail = models.CharField(max_length=1000)
        date_created = models.DateField(auto_now=True)
        completed = models.BooleanField(default=False)
  ```
- Create *Serializers* class  
    ```
    from .models import Task
    from rest_framework import serializers  

    class TaskSerializers(serializers.ModelSerializer):
        class Meta:
            model = Task
            fields = ('id', 'name', 'detail', 'date_created', 'completed')
  ```

- Create *ViewSet* class
    ```
    from rest_framework import viewsets
    from .serializer import TaskSerializers
    from .models import Task
    
    class TaskViewSet(viewsets.ModelViewSet):
        queryset = Task.objects.all().order_by('-date_created')
        serializer_class = TaskSerializers
  ```

- Update **settings.py**
    ```
    INSTALLED_APPS = [
    'rest_framework',
    'restapp',
    ...
    ...
    ]
  ```
  
- Update **urls.py**
    ```
    ...
    from rest_framework import routers
    from restapp import views
    
    router = routers.DefaultRouter()
    router.register(r'task',views.TaskViewSet)
    
    urlpatterns = [
        ...
        url(r'^', include(router.urls)),
    ]
  ```  
  
- Result:

  ![Preview ss_1](images/ss_1.png)
      
### Creating API Endpoints
- Create more *ViewSet*

    ```
    class DueTaskViewSet(viewsets.ModelViewSet):
        queryset = Task.objects.all().order_by('-date_created').filter(completed=False)
        serializer_class = TaskSerializers
  ```
    ```
    class CompletedTaskViewSet(viewsets.ModelViewSet):
        queryset = Task.objects.all().order_by('-date_created').filter(completed=True)
        serializer_class = TaskSerializers
  ```  
  
 - Update **urls.py**
    
    ```
    ...
    from restapp import views
    ...
    # router = routers.DefaultRouter()
    router = routers.SimpleRouter()
    ...
    router.register(r'due_task',views.DueTaskViewSet)
    router.register(r'completed_task',views.CompletedTaskViewSet)
    ...
   ``` 
  
- Result
    - Due Task API:
        
        ![Preview ss_2](images/ss_2.png)
        
    - Completed Task API
    
        ![Preview ss_3](images/ss_3.png)
            
### Adding Image Field    
- Install **pillow**

    ```
  pip install pillow
  ```                
  
- Add *ImageField* to the model  

    ```
    class Task(models.Model):
        ...
        image = models.ImageField(upload_to='images', default="images/default.jpg")
    ```
  
- Update **serializer.py**

    ```
    class TaskSerializers(serializers.ModelSerializer):
        image = serializers.ImageField(max_length=None, use_url=True)

        class Meta:
            ...
            fields = ('id', 'name', 'detail', 'date_created', 'completed', 'image')
    ```
  
- Add *MEDIA_ROOT* and *MEDIA_URL* to **settings.py**

    ```
    ...
    ...
    MEDIA_ROOT = os.path.join(BASE_DIR,'media')
    MEDIA_URL = '/media/'
  ```  
  
- Add media url to **urls.py**

    ```
    ...
    from django.conf.urls.static import static
    from django.conf import settings
    ...
    urlpatterns = [
                  ...
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
  ```
  
- Result   
    - Add new Task with image
    
        ![Preview ss_4](images/ss_4.png)
        
    - Open image via image fields in json
    
        ![Preview ss_5](images/ss_5.png)
        
### Filtering
- Install **Django Filter**
    ```
    pip install django-filter
  ```
      
- Add *django_filters* to **setting.py** 

    ```
    INSTALLED_APPS = [
        ...
        'django_filters',
    ]
  ``` 
  
- Add filter to the *ViewSet*   
    
    ```
    ...
    from rest_framework import filters
    ...
    class TaskViewSet(viewsets.ModelViewSet):
        ...
        filter_backends = (filters.DjangoFilterBackend, 
                           filters.OrderingFilter)
        filter_fields = ('completed',)
        ordering = ('-date_created',)
  ```   

- Result
    
    ![Preview ss_6](images/ss_6.png)
    
### Search functionality
- Add search filter to the *ViewSet*   
    
    ```
    ...
    from rest_framework import filters
    ...
    class TaskViewSet(viewsets.ModelViewSet):
        ...
        filter_backends = ( ... ,
                           filters.SearchFilter)
        filter_fields = ('completed',)
        ordering = ('-date_created',)
  ```   

- Result
    
    ![Preview ss_7](images/ss_7.png)
    
### *DefaultRouter* vs *SimpleRouter*
- DefaultRouter is similar to SimpleRouter,<br />
    but additionally includes a default API root view
- SimpleRouter

    ![Preview ss_8](images/ss_8.png)
    
- DefaultRouter

    ![Preview ss_9](images/ss_9.png)
    
### API Authentication
- Create new serializer for User model

    ```
    ...
    from django.contrib.auth import get_user_model
    ...
    class UserSerializers(serializers.ModelSerializer):
        password = serializers.CharField(write_only=True)
    
        def create(self, validated_data):
            user = get_user_model().objects.create(
                username=validated_data['username']
            )
            user.set_password(validated_data['password'])
            user.save()
            return user
    
        class Meta:
            model = get_user_model()
            fields = ('username', 'password')
    ...
  ```     
  
- Add authentication to the ViewSet

    ```
    ...
    from rest_framework.permissions import IsAuthenticated
    ...
    class TaskViewSet(viewsets.ModelViewSet):
        permission_classes = (IsAuthenticated,)
    ...
  ``` 

- Create register view
    
    ```
    ...
    from rest_framework.permissions import IsAuthenticated, AllowAny
    from django.contrib.auth import get_user_model  
    from rest_framework.generics import CreateAPIView
    ...
    class CreateUserView(CreateAPIView):
        model = get_user_model()
        permission_classes = (AllowAny,)
        serializer_class = UserSerializers
    ...
  ```  
  
- Add new urls to **urls.py**
    
    ```
    urlpatterns = [
              url(r'^register/$', views.CreateUserView.as_view(), name="register"),
              url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                ...
  ```  

- Result 
    - Register screen
    
        ![Preview ss_10](images/ss_10.png)
    
    - Login Screen
    
        ![Preview ss_11](images/ss_11.png)

    - Task api 
        - User is not logged in
        
            ![Preview ss_12](images/ss_12.png)
        
        - User is logged in
    
            ![Preview ss_13](images/ss_13.png)

      