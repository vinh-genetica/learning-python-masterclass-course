from django.db import models


# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=1000)
    detail = models.CharField(max_length=1000)
    date_created = models.DateField(auto_now=True)
    completed = models.BooleanField(default=False)
    image = models.ImageField(upload_to='images', default="images/default.jpg")

    def __str__(self):
        return str(self.date_created) + " | " + self.name
