Django==1.11.4
django-filter==1.0.4
djangorestframework==3.6.4
olefile==0.46
Pillow==4.2.1
pytz==2020.1
