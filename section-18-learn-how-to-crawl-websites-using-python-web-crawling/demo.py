from urllib.request import urlopen
from bs4 import BeautifulSoup

# html = urlopen("https://vi.wikipedia.org/wiki/Wikipedia")
# print(html.read())
#
# bsObject = BeautifulSoup(html.read(), "html.parser")
# print(bsObject.h1)

import os


def create_project_dir(directory):
    if not os.path.exists(directory):
        print("Creating new directory :" + directory)
        os.makedirs(directory)


# create_project_dir("theirsite")


def create_data_file(project_name, base_url):
    queue = os.path.join(project_name, 'queue.txt')
    crawled = os.path.join(project_name, 'crawled.txt')

    if not os.path.isfile('queue.txt'):
        write_file(queue, base_url)

    if not os.path.isfile('crawled.txt'):
        write_file(crawled, '')


def write_file(path, data):
    with open(path, 'w', encoding='utf-8') as f:
        f.write(data)


def append_file(path, data):
    with open(path, 'a', encoding='utf-8') as f:
        f.write(data, '\n')


def delete_file_contents(path):
    open(path, 'w', encoding='utf-8').close()


def file_to_set(file_name):
    results = set()
    with open(file_name, 'rt', encoding='utf-8') as f:
        for line in f:
            results.add(line.replace('\n', ""))
    return results


def set_to_file(links, file_name):
    with open(file_name, 'w', encoding='utf-8') as f:
        for link in sorted(links):
            f.write(link + '\n')
