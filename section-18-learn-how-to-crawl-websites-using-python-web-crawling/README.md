# Section 18: Learn How To Crawl Websites Using Python : Web Crawling

## Overview:
- Getting html code of a web page
- Using BeautifulSoup4 
- Web crawler introduction
- Building a web crawler step by step
    - Create new directory 
    - Create queue and crawled file
    - Create append and delete file contents function
    - Create convert *file_to_set* and *set_to_file* function
    - Create **link_finder.py**
    - Create **spider.py**
    - Create **domain.py**
    - Create **main.py**
### Getting html code of a web page
- Code
    ```
    from urllib.request import urlopen
    
    html = urlopen("https://vi.wikipedia.org/wiki/Wikipedia")
    print(html.read())
    ```

- Result: Whole html code of the web is printed

  ![Preview ss_01](images/ss_01.png)
  
### BeautifulSoup4
- Get specific tag of raw html code 
    ```
    from urllib.request import urlopen
    from bs4 import BeautifulSoup

    html = urlopen("https://vi.wikipedia.org/wiki/Wikipedia")
    # print(html.read())
    bsObject = BeautifulSoup(html.read(), "html.parser")
    print(bsObject.h1)
    ```

- Result:

  ![Preview ss_02](images/ss_02.png)
  
### Building a web crawler 
#### Create new directory 
- Code 
    ```
    import os

    def create_project_dir(directory):
        if not os.path.exists(directory):
            print("Creating new directory :" + directory)
            os.makedirs(directory)
    
    create_project_dir("theirsite")
    ```

- Result:

  ![Preview ss_03](images/ss_03.png)  

#### Create queue and crawled file
- Code 
    ```
    def create_data_file(project_name, base_url):
        queue = os.path.join(project_name, 'queue.txt')
        crawled = os.path.join(project_name, 'crawled.txt')
    
        if not os.path.isfile('queue.txt'):
            write_file(queue, base_url)
    
        if not os.path.isfile('crawled.txt'):
            write_file(crawled, '')

    def write_file(path, data):
        with open(path, 'w') as f:
            f.write(data)
    ```
  
#### Create append and delete file contents function
- Code 
    ```
    def append_file(path, data):
        with open(path, 'a') as f:
            f.write(data, '\n')
    
    def delete_file_contents(path):
        open(path, 'w').close()
    ```  
  
#### Create convert *file_to_set* and *set_to_file* function
- Code 
    ```
    def file_to_set(file_name):
        results = set()
        with open(file_name, 'rt') as f:
            for line in f:
                results.add(line.replace('\n', ""))
        return results
    
    
    def set_to_file(links, file_name):
        with open(file_name, 'w') as f:
            for link in sorted(links):
                f.write(link + '\n')
    ```    
#### Create link finder class
- Crawler class
- Using for get crawl data
- [link_finder.py](link_finder.py)
#### Create spider.py
- Function:
    - *boot* : Create required file and directory for the application
    - *crawl_page* : Crawl data , handling queue/crawled links 
    - *gather_links* : get all links from crawling website
    - *add_links_to_queue* : add new link to the queue list
    - *update_files* : Write queue/crawled link from memory to file
- [spider.py](spider.py)

#### Create domain.py
- Using for get domain and sb-domain of the website
- [domain.py](domain.py)

#### Create main.py
- Using to create, and handle crawling thread
- Main file of the crawler application
- Storage info of the app :
    - PROJECT_NAME
    - HOMEPAGE
    - DOMAIN_NAME
    - QUEUE_FILE
    - CRAWLED_FILE
    - NUMBER_OF_THREAD
- [main.py](main.py)

#### Result:
- Crawling all links from the web page

    ![Preview ss_03](images/ss_03.png)
    
- All link crawled is saved to **crawled.txt**

    ![Preview ss_04](images/ss_04.png)    