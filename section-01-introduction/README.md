# Section 1: Introduction

## Course Overview
- **Instructor:** [Ashutosh Pawar](https://www.udemy.com/user/a9ff8aeb-0700-4b60-950d-ffdce7bf69bc/)

## What in the course: 

### Python
- Basic syntax

### Tkinter

### Django

### Flask

### Web scraping
- How to crawling data from a website 
