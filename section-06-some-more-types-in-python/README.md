# Section 6: Some More Types In Python

## Overview:
- Dictionaries
- Tuples
    - Similar to list but cant change the value (immutable)
- List slicing,comprehension
- String formatting, string functions
- Numeric functions

###List Comprehension
Input
>list = [x ** 2 for x in range(10) if x % 2 == 0]
>
>print(list)

Output: 
>[0, 4, 16, 36, 64]

### String functions
- **join()**
- **replace()**
- **startwith()**, **endwith()**
- **upper()**, **lower()**

### Numeric functions
- **min()**, **max()**
- **abs()**

## Coding Challenge part 4